package controleur;

import java.util.HashMap;

import javafx.collections.ListChangeListener;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import moteur.acteurs.Acteur;
import moteur.acteurs.ObjetDynamiqueReactif;
import moteur.acteurs.PNJ;
import moteur.acteurs.Projectile;
import vue.PNJView;
import vue.PersonnageView;
import vue.ProjectileView;

public class EcouterListeEnnemis implements ListChangeListener<Acteur>{
	
	private HashMap <Acteur, ImageView> mapActeurDonneView;
	private Pane pane;

	public EcouterListeEnnemis(HashMap<Acteur, ImageView> h, Pane p) {
		this.mapActeurDonneView=h;
		this.pane=p;
	}
	@Override
	public void onChanged(Change<? extends Acteur> e) {
		while(e.next()) {
			for(Acteur remitem :  e.getRemoved()) {
				this.pane.getChildren().remove(this.mapActeurDonneView.get(remitem));
				this.mapActeurDonneView.remove(remitem);
			}
			
			for(Acteur remitem : e.getAddedSubList()) {
				if(remitem instanceof Projectile) {
					PersonnageView p = new ProjectileView((Projectile)remitem);
					this.mapActeurDonneView.put(remitem,p );
					this.pane.getChildren().add(p);
				}
				else if(remitem instanceof PNJ) {
					PNJView p = new PNJView((ObjetDynamiqueReactif)remitem);
					this.mapActeurDonneView.put(remitem, p);
					this.pane.getChildren().add(p);
				}
			}
	
		}
	}
}