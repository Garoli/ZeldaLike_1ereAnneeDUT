package controleur;

import moteur.*;
import moteur.acteurs.*;
import moteur.exceptions.LigneTropGrande;
import moteur.exceptions.LigneTropPetite;
import vue.*;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.fxml.Initializable;

public class Controleur implements Initializable {

	@FXML
	private Pane root;

	@FXML
	private PaneQuiBougeView paneQuiBouge;
	@FXML
	private TilePane tileMap;
	@FXML
	private Pane persopane;
	
	@FXML 
	private Pane paneplan1;
	
	@FXML
	private Pane paneInit;
	@FXML
	private Pane paneCommentJouerInit;
	
	@FXML
	private void commentJouerInit(ActionEvent event) {
		this.paneCommentJouerInit.setVisible(true);
	}
	@FXML
	private void playGame(ActionEvent event) {
		this.lancementJeu();
		this.stage.setResizable(true);
	}
	@FXML
	private void retourMenu(ActionEvent event) {
		this.paneCommentJouerInit.setVisible(false);
	}
	
	@FXML
	private Pane menu;
	@FXML 
	private Pane paneCommentJouerMenu;
	@FXML
	private void commentJouerMenu(ActionEvent event) {
		this.setAffichage(this.paneCommentJouerMenu);
	}
	
	@FXML
	private Pane paneGameOver;
	@FXML
	private Pane paneCreditsGameOver;
	@FXML
	private void afficherCreditsGameOver(ActionEvent event) {
		this.setAffichage(this.paneCreditsGameOver);
	}
	@FXML
	private void revenirPaneGameOver(ActionEvent event) {
		this.paneCreditsGameOver.setVisible(false);
	}
	
	@FXML
	private Pane paneGameWon;
	@FXML
	private Pane paneCreditsGameWon;
	@FXML
	private void afficherCreditsGameWon(ActionEvent event) {
		this.setAffichage(this.paneCreditsGameWon);
	}
	@FXML
	private void revenirPaneGameWon(ActionEvent event) {
		this.paneCreditsGameWon.setVisible(false);
	}
	
	@FXML
	private MessageView message;
	@FXML
	private Stage stage;
	
	private Jeu jeu;
	private Timeline gameLoop;
	private EcouterListeEnnemis EcouterListeEnnemis;
	private HashMap<Acteur,ImageView> listePersoEtView;
	private SoweliView persoView ;

	private boolean jeuEnCoursDExecution;
	private boolean gameWon;



	@FXML
	private void quitter(ActionEvent event) {
		this.stage.close();
	}
	@FXML
	private void rejouer(ActionEvent event) {
		this.persopane.getChildren().clear();
		this.paneplan1.getChildren().clear();
		this.paneGameOver.setVisible(false);
		this.paneGameWon.setVisible(false);
		this.lancementJeu();
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.paneInit.setVisible(true);
	}

	public void lancementJeu() {
		try {
			System.out.println("ATTENTION LES TOUCHES SONT  : \n Z POUR MONTER, Q A GAUCHE, D A DROITE ET S DESCENDRE !");
			this.jeu = Fabrique.fabriqueDeJeuMap2();
				
			System.out.println("Jeu fabriqué");
			this.paneQuiBouge.constructeurDifféré(jeu, root);
			this.listePersoEtView = new HashMap<>();

			Fabrique.fabriqueDeControleur(this.jeu, this.persopane, this.paneplan1, this.persoView, this.tileMap, this.listePersoEtView);

			this.persoView = new SoweliView(this.jeu.getSoweli());
			this.persopane.getChildren().add(this.persoView);
			this.paneplan1.getChildren().add(this.persoView.getBareVie());

			this.paneplan1.getChildren().add(new vueEtat (root, this.jeu.getSoweli()));
			this.paneplan1.getChildren().add(new VueObjetItem(root, this.jeu.getSoweli()));

			this.message = new MessageView(jeu, root);
			this.message.setVisible(false);
			this.paneplan1.getChildren().add(this.message);

			this.EcouterListeEnnemis = new EcouterListeEnnemis(this.listePersoEtView, this.persopane);
			this.jeu.getListeEnnemi().addListener(this.EcouterListeEnnemis);
			this.jeu.getListeObjetStatique().addListener(this.EcouterListeEnnemis);
			this.jeu.getListeObjetDynamique().addListener(this.EcouterListeEnnemis);

			this.paneQuiBouge.setVisible(true);
			this.paneplan1.setVisible(true);
			this.paneInit.setVisible(false);

			this.initAnimation();
			this.jeuEnCoursDExecution = true;
			this.gameWon = false;
			gameLoop.play();
			
		} catch (LigneTropGrande e) {
			this.afficheAlerteJeuNonCréé(e);
		}
		catch(LigneTropPetite e){
			this.afficheAlerteJeuNonCréé(e);
		}
	}
	
	private void setAffichage(Pane p) {
		p.setTranslateX((root.getWidth()-p.getWidth())/2);
		p.setTranslateY((root.getHeight()-p.getHeight())/2);
		p.setVisible(true);
	}
	
	private void afficheAlerteJeuNonCréé(Exception e) {
		Alert a=new Alert(AlertType.ERROR);
		a.setTitle("ERREUR");
		a.setHeaderText("Le jeu n'a pas pu être créé");
		a.setContentText("Erreur : "+ e);
		a.showAndWait();
	}
	
	private void initAnimation() {
		gameLoop = new Timeline();
		gameLoop.setCycleCount(Timeline.INDEFINITE);
		KeyFrame kf = new KeyFrame(Duration.seconds(0.08), 
				(ev ->{
					if(! this.gameWon) {
						this.jeu.gameLoop();
						this.finDuJeu();
						this.message.màj();
					}
					else {
						this.jeuEnCoursDExecution = false;
						if(! this.jeu.gameWonLoop()) {
							this.gameLoop.pause();
							this.setAffichage(paneGameWon);
						}
					}
				}));
		gameLoop.getKeyFrames().add(kf);
	}

	private void finDuJeu() {

		if(this.jeu.isGameLost()) {
			this.gameLost();
		}

		if(this.jeu.isGameWon()) {
			this.gameWon = true;
		}
	}

	private void gameLost() {
		this.gameLoop.pause();
		this.setAffichage(paneGameOver);
	}

	public void setStage(Stage s) {
		this.stage=s;
	}

	public void touchepresse(KeyEvent evt) {
		if(this.jeuEnCoursDExecution) {
			switch(evt.getCode()) {
			case Z :
			case UP : 
				this.jeu.getSoweli().monter();
				break;
			case S :
			case DOWN : 
				this.jeu.getSoweli().descendre();
				break;
			case Q :
			case LEFT : 
				this.jeu.getSoweli().gauche();
				break;
			case D :
			case RIGHT :
				this.jeu.getSoweli().droite();
				break;
			case J :
			case SPACE : 
				this.jeu.getSoweli().setEnAction(true);
				break;
			case K :
			case X :
				this.jeu.getSoweli().incrementeEtat();
				break;
			case ESCAPE :
				if(this.jeuEnCoursDExecution) {
					this.jeuEnCoursDExecution=false;
					this.gameLoop.pause();
					this.setAffichage(menu);
				}
				else {
					this.paneCreditsGameOver.setVisible(false);
					this.paneGameOver.setVisible(true);
				}
				break;
			default:
				break;
			}
		}
		else {
			switch(evt.getCode()) {
			case ESCAPE :
				this.jeuEnCoursDExecution=true;
				this.gameLoop.play();
				this.menu.setVisible(false);
				this.paneCommentJouerInit.setVisible(false);
				this.paneCommentJouerMenu.setVisible(false);
				break;
			default:
				break;
			}
		}
	}
	public void touchetapee(KeyEvent evt) {
		this.touchepresse(evt);
	}
	public void toucherelachee() {
		this.persoView.changementImagePersoStatique();
		this.jeu.getSoweli().setEnMouvement(false);
		this.jeu.getSoweli().setEnAction(false);
	}
}