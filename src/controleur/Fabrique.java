package controleur;

import java.util.HashMap;


import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import moteur.*;
import moteur.acteurs.*;
import moteur.déplacements.*;
import moteur.etats.*;
import moteur.exceptions.LigneTropGrande;
import moteur.exceptions.LigneTropPetite;
import vue.*;

public class Fabrique {

	public static Jeu fabriqueDeJeuMap1() throws Exception {
		Jeu jeu = new Jeu("src/ressources/text/matrice-map1.txt");

		Soweli soweli = new Soweli(jeu,0,0,0,12);
		jeu.setSoweli(soweli);
		jeu.getCollision().addSoweli(soweli);

		Souris souris = new Souris(jeu,6*48,6*48,12,0);
		jeu.addEnnemi(souris);

		jeu.getCollision().màJListeObjets();

		return jeu;
	}

	public static Jeu fabriqueDeJeuMap2() throws LigneTropPetite, LigneTropGrande {
		Jeu jeu = new Jeu("src/ressources/text/matrice-map4.txt");

		Soweli soweli = new Soweli(jeu,336,48,0,12);

		//Pour finir en deux deux
		//Soweli soweli = new Soweli(jeu, 37*48, 4*48, 0, 12);
		//jeu.addObjetStatique(new Clef(jeu, 1850, 200));

		soweli.setEtat(new EtatExploration(jeu));
		soweli.addEtat(soweli.getEtat());
	
		Déplacement d = new DéplacementBase(jeu, soweli);
		soweli.setDéplacement(d);
		jeu.setSoweli(soweli);

		jeu.makeBFS();
		jeu.getCollision().addSoweli(soweli);

		jeu.addEnnemi(new Souris(jeu,240,1488,0,12));
		jeu.addEnnemi(new Souris(jeu,192,2688,0,12));
		jeu.addEnnemi(new Souris(jeu,48,1920,12,0));
		jeu.addEnnemi(new Souris(jeu,1,2064,12,0));
		jeu.addEnnemi(new Souris(jeu,96,2208,12,0));
		jeu.addEnnemi(new Souris(jeu,1728,1776,12,0));

		jeu.addEnnemi(new Belette( jeu, 1824, 2112, 12, 0));
		jeu.addEnnemi(new Belette( jeu, 2160, 2304, 12, 0));
		jeu.addEnnemi(new Belette( jeu, 1824, 336, 12, 0));
		jeu.addEnnemi(new Belette( jeu, 1920, 768, 12, 0));
		jeu.addEnnemi(new Belette( jeu, 2400, 1536, 12, 0));
		jeu.addEnnemi(new Belette( jeu, 2448, 1632, 12, 0));
		jeu.addEnnemi(new Belette( jeu, 1056, 2688, 12, 0));
		jeu.addEnnemi(new Belette( jeu, 672, 2448, 12, 0));
		
		jeu.addEnnemi(new Chien(jeu,1824, 240, 12, 0));
		jeu.addEnnemi(new Chien(jeu,2736, 2976, 12, 0));
		jeu.addEnnemi(new Chien(jeu,2784, 48, 12, 0));
		jeu.addEnnemi(new Chien(jeu,96, 1680, 12, 0));

		jeu.addObjetStatique(new Clef(jeu, 1, 1728));

		jeu.addObjetStatique(new Fleur(jeu, 48, 2736));
		jeu.addObjetStatique(new Fleur(jeu, 576, 2112));
		jeu.addObjetStatique(new Fleur(jeu, 2304, 1440));
		jeu.addObjetStatique(new Fleur(jeu, 1776, 96));
		jeu.addObjetStatique(new Fleur(jeu, 1296, 2160));

		jeu.addObjetDynamique(new Caillou(jeu,2736, 2448));
		jeu.addObjetDynamique(new Caillou(jeu,1104, 1392));
		jeu.addObjetDynamique(new Caillou(jeu,1056, 1440));
		jeu.addObjetDynamique(new Caillou(jeu,2784, 2496));
		jeu.addObjetDynamique(new Caillou(jeu,2976, 528));
		jeu.addObjetDynamique(new Caillou(jeu,3024, 528));
		jeu.addObjetDynamique(new Caillou(jeu,336, 672));
		jeu.addObjetDynamique(new Caillou(jeu,288, 768));

		jeu.addObjetDynamique(new PNJEtat(jeu, 432, 1056, 12, 0,"Tu peux maintenant donner des coups de patte  ! ", new EtatCoupDePatte(jeu)));
		jeu.addObjetDynamique(new PNJEtat(jeu, 2208, 2928, 12, 0, "Tu peux maintenant cracher ! ", new EtatEnvoieProjectile(jeu)));
		jeu.addObjetDynamique(new PNJEtat(jeu, 2408,1008, 12, 0, "Tu peux maintenant nager !", new EtatNager(jeu)));
		jeu.addObjetDynamique(new PNJPousser(jeu, 1200, 864 ,12, 0, "Tu peux maintenant pousser des obstacles !"));
		jeu.addObjetDynamique(new PNJCoeur(jeu, 1344, 1728, 12, 0, "Je te donne mon coeur !"));
		jeu.addObjetDynamique(new PNJCoeur(jeu, 1200, 2880, 12, 0, "Un petit bisou magique !"));
		jeu.addObjetDynamique(new PNJCoeur(jeu, 2736, 48, 12, 0, "Voici un peu de vie !"));
		jeu.addObjetDynamique(new PNJ(jeu, 576, 384, 12, 0, "Bêêêêêeeeeeeeeeeee ! "));
		jeu.addObjetDynamique(new PNJ(jeu, 912, 2880, 12, 0, "Pleins de moutons sont enfermés dans la maison au loin ..."));
		jeu.addObjetDynamique(new PNJ(jeu, 2736, 2304, 12, 0, "Fais attention au feu ! "));
		jeu.addObjetDynamique(new PNJ(jeu, 864, 1872, 12, 0, "Les fleurs c'est bon pour la santé ;) "));
		jeu.addObjetDynamique(new PNJ(jeu, 1968, 192, 12, 0, "Je ne retrouve plus papa et maman ! J'ai peur. "));
		jeu.addObjetDynamique(new PNJ(jeu, 2880, 2016, 12, 0, "Bêêêêêeeee "));
		jeu.addObjetDynamique(new PNJ(jeu, 960, 96, 12, 0, "Tu as des cornes violettes ! tu es l'élu ! "));
		jeu.addObjetDynamique(new PNJ(jeu, 192, 528, 12, 0, "Où est Charlie ? "));
		jeu.addObjetDynamique(new PNJ(jeu, 2400,720, 12, 0, "Trouve la clef et positionne toi devant la porte"));
		jeu.addObjetDynamique(new PNJ(jeu, 720, 2640, 12, 0, "Bêêê !"));
		jeu.addObjetDynamique(new PNJ(jeu, 912, 432, 12, 0, "Les belettes te poursuivront seulement si tu es en face d'elles. "));
		jeu.addObjetDynamique(new PNJ(jeu, 144,288, 12, 0, "Les chiens te suivent à la trace... Il parait qu'ils ont l'odorat très développé. "));

		jeu.addEnnemi(new Feu(jeu, 1632,1104)); 
		jeu.addEnnemi(new Feu(jeu, 1584,1152));
		jeu.addEnnemi(new Feu(jeu, 1584,1200));
		jeu.addEnnemi(new Feu(jeu, 1632,1200));
		jeu.addEnnemi(new Feu(jeu, 1632,1248));
		jeu.addEnnemi(new Feu(jeu, 1680,1200));
		jeu.addEnnemi(new Feu(jeu, 1728,1152));
		jeu.addEnnemi(new Feu(jeu, 1728,1104));
		jeu.addEnnemi(new Feu(jeu, 1776,1200));
		jeu.addEnnemi(new Feu(jeu, 1776,1152));
		jeu.addEnnemi(new Feu(jeu, 1824,1200));

		jeu.getCollision().màJListeObjets();

		return jeu;
	}

	public static void fabriqueDeControleur(Jeu jeu, Pane persopane, Pane paneplan1, SoweliView sw, TilePane tileMap, HashMap<Acteur,ImageView> listePersoEtView) {

		tileMap.setPrefRows(jeu.getMap().getHauteur());
		tileMap.setPrefColumns(jeu.getMap().getLargeur());

		Tile[][] terrainphysique = jeu.getMap().getTerrain();

		for (int i=0; i< terrainphysique.length; i++) {
			for (int j=0; j < terrainphysique[0].length; j++) {
				ImageView img = new ImageView("file:src/ressources/tiles/" + terrainphysique[i][j].getID() + ".png");
				img.setFitHeight(48);
				img.setFitWidth(48);
				tileMap.getChildren().add(img);
			}
		}

		PersonnageView eView;

		for(Ennemi e : jeu.getListeEnnemi()) {
			if (e instanceof Souris) {
				eView = new SourisView(e);
			}
			else if (e instanceof Belette) {
				eView = new BeletteView(e);
			}
			else if (e instanceof Feu) {
				eView = new PersonnageView(0,0,e,"file:src/ressources/tiles/66.png");
			}
			else /*if (e instanceof Chien)*/ {
				eView = new ChienView(e);
			}

			listePersoEtView.put(e, eView);
			persopane.getChildren().add(eView);
		}

		VueObjetStatiqueSprite osView;

		for(ObjetStatique os : jeu.getListeObjetStatique()) {
			if (os instanceof Clef) {
				osView = new ClefView(os);
			}
			else /*if (e instanceof Fleur)*/{
				osView = new FleurView(os);
			}

			listePersoEtView.put(os, osView);
			persopane.getChildren().add(osView);
		}

		for(ObjetDynamiqueReactif o : jeu.getListeObjetDynamique()) {
			VueObjetDynamique oView;
			if(o instanceof ObjetDeplacable) {
				oView = new CaillouView(o);
			}
			else {
				oView = new PNJView(o);
			}
			persopane.getChildren().add(oView);
		}
	}
}
