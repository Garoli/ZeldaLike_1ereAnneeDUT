package vue;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;

public class VueObjetStatiqueSprite extends ImageView{

	private int colonneTile;
	private int nbTile;
	
	public VueObjetStatiqueSprite(int nbTile, String adresseSpriteSheet) {
		super(adresseSpriteSheet);
		this.colonneTile=0;
		this.nbTile=nbTile;
		this.changementTile();

	}
	
	public void changementTile() {
		this.setViewport(new Rectangle2D(this.colonneTile*48,0,48,48));
		colonneTile++;			
		if(this.colonneTile==this.nbTile) {
			this.colonneTile=0;
		}
	}
	
	public void changementTile(int c) {
		this.colonneTile=c;
		this.setViewport(new Rectangle2D(this.colonneTile*48,0,48,48));
	}

}
