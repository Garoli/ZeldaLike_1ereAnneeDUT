package vue;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import moteur.Jeu;

public class MessageView extends Label {
	
	private Jeu jeu;
	private int temps;
	
	public MessageView(Jeu jeu, Pane root) {
		this.jeu=jeu;
		this.temps=0;
		
		this.setVisible(false);
		this.setText("Regarde, tu peux maintenant pousser des objets ! ");
		//this.setLayoutX(91);
		//this.setLayoutY(84);
		this.setPrefHeight(65);
		this.setPrefWidth(root.getWidth()*0.8);
		this.setStyle("-fx-background-color: lemonchiffon; -fx-border-color: darkblue; -fx-border-width: 3; -fx-border-radius: 15; -fx-background-radius: 15;");
		this.setPadding(new Insets(10));
		this.setFont(new Font("FreeSans Bold", 15));
		
		this.jeu.getMessageProperty().addListener((observable, oldValue, newValue) -> {
			this.setVisible(true);
			this.setText(newValue);
			this.temps=40;

			this.setTranslateX((root.getWidth() - this.getWidth()) / 2);
			this.setTranslateY((root.getHeight() - this.getHeight() - 48));
		});
	}
	
	public void màj() {
		this.temps--;
		if(temps==0) {
			this.setVisible(false);
		}
	}

	
}
