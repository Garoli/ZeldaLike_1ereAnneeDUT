package vue;

import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import moteur.acteurs.Soweli;

public class VueBareVie extends HBox {
	
	private Soweli soweli;
	
	public VueBareVie(Soweli soweli) {
		super();
		this.soweli=soweli;
		this.soweli.getPVProperty().addListener((observable, oldValue, newValue) -> {
			this.changementCoeur(newValue.intValue());
		});
		this.changementCoeur(this.soweli.getPV());
	}
	
	private void changementCoeur(int pv) {
		this.getChildren().clear();
		for (int i=0; i < soweli.getPVMax()/2; i++) {
			if ( pv >= 2) {
				ImageView coeurPlein = new ImageView("file:src/ressources/utilisateur/coeurentier.png");
				this.getChildren().add(coeurPlein);
			}
			
			else if (pv == 1) {
				ImageView demiCoeur = new ImageView("file:src/ressources/utilisateur/coeurmoitier.png");
				this.getChildren().add(demiCoeur);
			}
			
			else {
				ImageView coeurVide = new ImageView("file:src/ressources/utilisateur/coeurentierpasdevie.png");
				this.getChildren().add(coeurVide);
			}
			
			pv -= 2;
		}
	}
}