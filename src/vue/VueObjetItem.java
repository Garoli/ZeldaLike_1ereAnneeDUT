package vue;


import javafx.scene.layout.Pane;
import moteur.acteurs.Soweli;

public class VueObjetItem extends VueObjetStatiqueSprite{
	
	
	private Soweli soweli;
	Pane root;
	public VueObjetItem(Pane pane, Soweli soweli) {
		super(2, "file:src/ressources/utilisateur/clef.png");
		this.root=pane;
		this.soweli=soweli;
		this.setTranslateY(96);
		
		this.soweli.getNbrItemProperty().addListener((observable, oldValue, newValue) -> {
			this.changementTile(newValue.intValue());
		});

	
		
	
	}

}