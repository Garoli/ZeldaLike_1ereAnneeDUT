package vue;

import moteur.acteurs.Personnage;

public class ProjectileView extends EnnemiView {

	public ProjectileView(Personnage perso) {
		super(2,4,perso, "file:src/ressources/acteurs/projectile.png");
	}

}
