package vue;

import moteur.acteurs.Personnage;

public class BeletteView extends EnnemiView {

	public BeletteView(Personnage perso) {
		super(3,4,perso, "file:src/ressources/acteurs/belette.png");
	}

}
