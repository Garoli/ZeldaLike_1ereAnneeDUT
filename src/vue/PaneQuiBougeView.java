package vue;

import javafx.scene.layout.Pane;
import moteur.Jeu;
import moteur.Map;
import moteur.acteurs.Soweli;

public class PaneQuiBougeView extends Pane{
	
	public PaneQuiBougeView() {
	}

	public void constructeurDifféré(Jeu jeu, Pane root) {
		Soweli soweli=jeu.getSoweli();
		Map map = jeu.getMap();
		
		this.setTranslateX(0);
		this.setTranslateY(0);
			
		soweli.getXProperty().addListener((observable, oldValue, newValue) -> {
			if(newValue.intValue() > root.getWidth()/2 && newValue.intValue() < ((map.getLargeur()*48) - (root.getWidth()/2))) {
				this.setTranslateX(root.getWidth()/2-newValue.intValue());
			}
		});
		
		soweli.getYProperty().addListener((observable, oldValue, newValue) -> {
			if(newValue.intValue() > root.getHeight()/2 && newValue.intValue() < ((map.getHauteur()*48) - (root.getHeight()/2))) {
				this.setTranslateY(root.getHeight()/2-newValue.intValue());
			}
		});
	}
}
