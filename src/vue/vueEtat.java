package vue;


import javafx.scene.layout.Pane;
import moteur.acteurs.Soweli;

public class vueEtat extends VueObjetStatiqueSprite{
	
	
	private Soweli soweli;
	Pane root;
	public vueEtat(Pane pane, Soweli soweli) {
		super(5, "file:src/ressources/utilisateur/SpriteEtats.png");
		this.root=pane;
		this.soweli=soweli;
		this.setTranslateY(48);
		
		
		this.soweli.getNumEtatProperty().addListener((observable, oldValue, newValue) -> {
			this.changementTile(newValue.intValue());

		});
	
		
	
	}

}
