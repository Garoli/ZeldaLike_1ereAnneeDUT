package vue;

import moteur.acteurs.Soweli;

public class SoweliView extends PersonnageView{

	private VueBareVie bareVie;
	private vueEtat vueEtat;
	private VueObjetItem vueObjetItem;

	public SoweliView(Soweli soweli) {
		super(3,4,soweli, "file:src/ressources/acteurs/soweli.png");
		this.bareVie= new VueBareVie(soweli);
		
		soweli.getEnMouvementProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue &&  ! soweli.getEnAction()) {
				this.changementImagePerso();
			}
		});
		soweli.getEnActionProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				if(soweli.getNumEtat()!=0){
					if(soweli.getNumEtat()!=3 || soweli.estDansLEau()) {
						this.changementImagePerso(soweli.getNumEtat()+2);
					}
				}	
			}
		});
	}

	public VueBareVie getBareVie() {
		return this.bareVie;
	}

	public vueEtat getVueEtat() {
		return this.vueEtat;
	}
	
	public VueObjetItem getObjetItem() {
		return this.vueObjetItem;
	}

	public void changementImagePerso(int colonne) {
		this.setLigne();
		this.setColonne(colonne);
		this.setViewport();
	}
	
	
}