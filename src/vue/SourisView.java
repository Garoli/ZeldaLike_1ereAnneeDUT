package vue;

import moteur.acteurs.Personnage;

public class SourisView extends EnnemiView{

	public SourisView(Personnage perso) {
		super(2,4,perso,"file:src/ressources/acteurs/souris.png");
	}

}
