package vue;

import moteur.acteurs.Personnage;

public class EnnemiView extends PersonnageView{

	public EnnemiView(int nbTile, int nbSprite, Personnage perso, String adresseSpreadSheet) {
		super(nbTile, nbSprite, perso, adresseSpreadSheet);

		this.perso.getEnMouvementProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				this.changementImagePerso();
			}
		});
	}
	

}
