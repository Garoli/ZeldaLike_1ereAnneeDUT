package vue;

import moteur.acteurs.Acteur;

public class VueObjetDynamique extends VueObjetStatiqueSprite {
	
	private Acteur objet;
	
	public VueObjetDynamique(int nbTile, Acteur objet, String adresseSpriteSheet) {
		super(nbTile, adresseSpriteSheet);
		this.objet=objet;
		this.translateXProperty().bind(this.objet.getXProperty());
		this.translateYProperty().bind(this.objet.getYProperty());
	}
}
