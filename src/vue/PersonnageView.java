package vue;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import moteur.acteurs.Personnage;

public class PersonnageView extends ImageView {

	protected Personnage perso;
	private int ligneSprite;
	private int colonneTile;
	private int nbTile;
	private int nbSprite;

	public PersonnageView(int nbTile,int nbSprite, Personnage perso, String adresseSpreadSheet) {
		super(adresseSpreadSheet);
		this.perso = perso;

		this.nbTile=nbTile;
		this.nbSprite=nbSprite;

		this.setLigne();
		this.colonneTile=0;
		this.setViewport();

		this.translateXProperty().bind(this.perso.getXProperty());
		this.translateYProperty().bind(this.perso.getYProperty());

	}

	protected void setViewport() {
		this.setViewport(new Rectangle2D(this.colonneTile*48,this.ligneSprite*48,48,48));
	}
	
	protected void setLigne() {
		int oX = this.perso.getOX();
		if(oX==0) {
			if(this.perso.getOY()<0)
				this.ligneSprite=0;
			else
				this.ligneSprite=1;
		} else {
			if (oX>0)
				this.ligneSprite=2;
			else
				this.ligneSprite=3;
		}
		
		if(this.nbSprite<=this.ligneSprite)
			this.ligneSprite=0;
	}
	
	protected void setColonne() {
		this.colonneTile++;			
		if(this.colonneTile>=this.nbTile) {
			this.colonneTile=0;
		}
	}
	
	protected void setColonne(int colonne) {
		this.colonneTile=colonne;
	}

	public void changementImagePerso() {
		this.setLigne();

		this.setColonne();

		this.setViewport();
	}
	
	public void changementImagePersoStatique() {
		this.colonneTile=0;
		this.setViewport();
	}

}