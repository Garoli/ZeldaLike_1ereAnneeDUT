package vue;

import moteur.acteurs.Personnage;

public class ChienView extends EnnemiView {

	public ChienView(Personnage perso) {
		super(3,4,perso, "file:src/ressources/acteurs/chien.png");
	}

}
