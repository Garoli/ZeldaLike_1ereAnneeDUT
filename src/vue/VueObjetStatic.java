package vue;

import moteur.acteurs.Acteur;

public class VueObjetStatic extends VueObjetStatiqueSprite {

	private Acteur objet;
	
	public VueObjetStatic(int nbTile, Acteur objet, String adresseSpriteSheet) {
		super(nbTile, adresseSpriteSheet);
		this.objet=objet;
		this.setTranslateX(this.objet.getX());
		this.setTranslateY(this.objet.getY());
	}
}
