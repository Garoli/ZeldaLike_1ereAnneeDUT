package moteur;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import moteur.acteurs.*;
import moteur.exceptions.LigneTropGrande;
import moteur.exceptions.LigneTropPetite;

public class Jeu {

	private Map map;
	private BFS bfs;
	private Soweli soweli;

	private ObservableList<Ennemi> listeEnnemi;
	private ObservableList<ObjetDynamiqueReactif> listeObjetDynamique;
	private ObservableList<ObjetStatique> listeObjetStatique;
	private SimpleStringProperty message;
	private Collision collision;

	private int temps;

	public Jeu(String adresse) throws LigneTropPetite, LigneTropGrande {
		this.map = new Map(adresse,70);
		this.collision = new Collision(this);
		this.bfs = new BFS(this);
		this.collision = new Collision(this);
		
		this.listeEnnemi = FXCollections.observableArrayList();
		this.listeObjetDynamique = FXCollections.observableArrayList();
		this.listeObjetStatique = FXCollections.observableArrayList();
		
		this.temps = 0;
		this.message = new SimpleStringProperty("");
	}

	public void gameLoop() {
		this.temps++;

		if(temps%3==0) {
			this.bfs.makeBFS();
		}

		this.soweli.agir();

		this.retirerMorts();
		this.ramasserObjets();

		for(Ennemi e : this.listeEnnemi) {
			e.agir();
		}
	}
	private void retirerMorts() {
		for(int i=0; i<this.listeEnnemi.size(); i++) {
			Ennemi e = this.listeEnnemi.get(i);
			if(e.getPV()<=0) {
				this.listeEnnemi.remove(e);
				this.collision.removeObjet(e);
			}
		}
	}
	private void ramasserObjets() {
		for(int i=0; i<this.listeObjetStatique.size(); i++) {
			ObjetStatique os = this.listeObjetStatique.get(i);
			if(this.soweli.getListeObjets().contains(os)) {
				if (os instanceof Fleur) {
					this.soweli.getListeObjets().remove(os);
				}
				this.listeObjetStatique.remove(os);
				this.collision.removeObjet(os);
			}
		}
	}

	public int getTemps() {
		return temps;
	}

	public Map getMap() {
		return this.map;
	}

	public Collision getCollision() {
		return this.collision;
	}

	public BFS getBFS() {
		return this.bfs;
	}

	public void makeBFS() {
		this.bfs = new BFS(this);
	}

	public void setSoweli(Soweli soweli) {
		this.soweli = soweli;
	}

	public Soweli getSoweli() {
		return this.soweli;
	}

	public void addEnnemi(Ennemi e) {
		this.listeEnnemi.add(e);
	}

	public Ennemi getEnnemi(int i) {
		return this.listeEnnemi.get(i);
	}

	public ObservableList<Ennemi> getListeEnnemi() {
		return this.listeEnnemi;
	}

	public void addObjetStatique(ObjetStatique os) {
		this.listeObjetStatique.add(os);
	}

	public ObjetStatique getObjetStatique(int i) {
		return this.listeObjetStatique.get(i);
	}

	public ObservableList<ObjetStatique> getListeObjetStatique() {
		return this.listeObjetStatique;
	}
	
	public void addObjetDynamique(ObjetDynamiqueReactif p) {
		this.listeObjetDynamique.add(p);
	}
	
	public ObservableList<ObjetDynamiqueReactif> getListeObjetDynamique() {
		return this.listeObjetDynamique;
	}

	public String getMessage() {
		return this.message.getValue();
	}

	public void setMessage(String message) {
		this.message.setValue(message);
	}

	public SimpleStringProperty getMessageProperty() {
		return this.message;
	}

	public boolean isGameWon() {
		if (Math.abs(this.soweli.getX() - 2300)>480 || Math.abs(this.soweli.getY() - 700)>200)
			return false;

		for (ObjetStatique o : this.soweli.getListeObjets())
			if(o instanceof Clef)
				return true;

		return false;
	}
	
	public boolean isGameLost() {
		return soweli.getPV()<=0;
	}

	public boolean gameWonLoop() {
		if (Math.abs(this.soweli.getY()-732)>12) {
			if(this.soweli.getY()<720+12) {
				this.soweli.descendre();
				this.soweli.agir();
			}
			else if(this.soweli.getY()>732) {
				this.soweli.monter();
				this.soweli.agir();
			}
			this.soweli.setEnMouvement(false);
		}
		else if(Math.abs(this.soweli.getX()-2304)>12){
			if(this.soweli.getX()<2304) {
				this.soweli.droite();
				this.soweli.agir();
			}
			else if(this.soweli.getX()>2304) {
				this.soweli.gauche();
				this.soweli.agir();
			}
			this.soweli.setEnMouvement(false);
		}
		else if(soweli.getX()!=2304 || soweli.getY()!=720) {
			soweli.setX(2304);
			soweli.setY(720);
		}
		else if(soweli.getOX()!=0 || soweli.getOY()!=-12) {
			soweli.setOX(0);
			soweli.setOY(-12);
			soweli.setEnMouvement(true);
		}
		else if(this.listeObjetDynamique.size()<70) {
			int x = (int) (1000  + Math.random()*2000);
			int y = (int) (Math.random()*1400);

			if(this.map.estAccessible(x, y) && this.collision.estLibre(new Tile(x/48, y/48, 0)));
			
			PNJ pnj = new PNJ(this, x, y, 0, 0, null);
			if(this.collision.peutSeDeplacer(pnj)) {
				this.listeObjetDynamique.add(pnj);
			}
			this.message.set("Hourra, nous sommes sauvés !");
		}
		else {
			return false;
		}
		return true;
		
	}

}