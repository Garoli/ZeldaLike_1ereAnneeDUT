package moteur.acteurs;

import moteur.Jeu;
import moteur.déplacements.DéplacementBase;

public class Caillou extends ObjetDeplacable {

	public Caillou(Jeu jeu, int x, int y) {
		super(jeu, x, y, 0, 0);
		this.setDéplacement(new DéplacementBase(jeu, this));
	}

}
