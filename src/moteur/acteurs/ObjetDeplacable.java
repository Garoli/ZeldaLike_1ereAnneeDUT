package moteur.acteurs;

import moteur.Jeu;
import moteur.déplacements.Déplacement;

public class ObjetDeplacable extends ObjetDynamiqueReactif {

	private Déplacement déplacement;
	
	public ObjetDeplacable(Jeu jeu, int x, int y, int oX, int oY) {
		super(jeu, x, y, oX, oY);
	}
	
	public void setDéplacement(Déplacement déplacement) {
		this.déplacement = déplacement;
	}
	
	public String toString() {
		return "caillou";
	}

	@Override
	public void reagir() {
		if (this.getJeu().getSoweli().getPeutPousser()) {
			setOXetOY(this.getJeu().getSoweli().getOX(),this.getJeu().getSoweli().getOY());
			this.déplacement.déplacer();
			setOXetOY(0, 0);	
		}
			
	}

}