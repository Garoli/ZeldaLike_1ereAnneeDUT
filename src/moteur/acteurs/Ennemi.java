package moteur.acteurs;

import moteur.Jeu;


public abstract class Ennemi extends Personnage{
	
	public Ennemi(Jeu jeu, int x, int y, int oX, int oY,int pv,int pointAttaque, int vitesse) {
		super(jeu, x, y,oX, oY, pv,pointAttaque,vitesse);
	}

	public void attaque() {
		if(this.getJeu().getCollision().tapeSoweli(this)) {
			this.getJeu().getSoweli().recoitDegat(this.getPtAttaque());
		}
	}
	
}
