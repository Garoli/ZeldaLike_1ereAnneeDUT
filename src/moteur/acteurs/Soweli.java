package moteur.acteurs;

import java.util.ArrayList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

import moteur.Jeu;
import moteur.acteurs.Personnage;
import moteur.déplacements.Déplacement;
import moteur.déplacements.DéplacementNage;
import moteur.etats.Etat;
import moteur.etats.EtatExploration;

public class Soweli extends Personnage{

	private ArrayList<ObjetStatique> listeObjets;
	private ArrayList<Etat> listeEtats;
	private Etat etat;
	private SimpleIntegerProperty numEtat;
	private SimpleIntegerProperty nbrItem;
	private SimpleBooleanProperty enAction;
	private Déplacement déplacementNage;
	
	private SimpleIntegerProperty pvMax;
	
	public Soweli(Jeu jeu, int x, int y, int oX, int oY) {
		super(jeu, x, y, oX, oY, 6,1,12);

		this.listeEtats = new ArrayList<>();
		this.listeObjets = new ArrayList<>();
		
		this.numEtat= new SimpleIntegerProperty(0);
		this.nbrItem = new SimpleIntegerProperty(0);
		this.pvMax = new SimpleIntegerProperty(this.getPV());
		this.enAction = new SimpleBooleanProperty(false);
		this.déplacementNage = new DéplacementNage(jeu, this);
	}
	
	public void monter() {
		this.setOXetOY(0, -getVitesse());
		this.setEnMouvement(true);
	}
	public void descendre() {
		this.setOXetOY(0, getVitesse());
		this.setEnMouvement(true);
	}
	public void gauche() {
		this.setOXetOY(-getVitesse(), 0);
		this.setEnMouvement(true);
	}
	public void droite() {
		this.setOXetOY(getVitesse(), 0);
		this.setEnMouvement(true);
	}
	
	public void agir() {
		this.etat.agir();
		this.setEnMouvement(false);
	}

	public void incrementeEtat() {
		int numEtat = this.getNumEtat() +1;
		
		if(numEtat>=this.listeEtats.size())
			numEtat=0;
			
		this.etat = this.listeEtats.get(numEtat);
		this.numEtat.set(numEtat);
	}
	
	public void addPV(int pv) {
		if ((this.getPV()+pv) < getPVMax()) {
			this.setPV(this.getPV()+pv);
		}
		else {
			this.setPV(getPVMax());
		}
	}
	
	public void addEtat(Etat e) {
		this.listeEtats.add(e);
	}

	public void setPeutPousser() {
		try {
			((EtatExploration)this.etat).setPeutPousser(true);
		} catch (Exception e) {
		}
	}
	
	public boolean getPeutPousser() {
		return ((EtatExploration)this.etat).getPeutPousser();
	}
	
	public void setPeutNager() {
		this.déplacementNage = new DéplacementNage(this.getJeu(), this);
	}
	
	public boolean estDansLEau() {
		int nbPointsDansLEau = 0;
		
		if (this.getJeu().getMap().estEau(this.getX(), this.getY()))
				nbPointsDansLEau++;
		if (this.getJeu().getMap().estEau(this.getX()+48, this.getY()))
				nbPointsDansLEau++;
		if (this.getJeu().getMap().estEau(this.getX(), this.getY()+47))
				nbPointsDansLEau++;
		if (this.getJeu().getMap().estEau(this.getX()+48, this.getY()+47))
				nbPointsDansLEau++;
		
		return nbPointsDansLEau > 2;
	}
	
	public void addObjetStatic(ObjetStatique os) {
		listeObjets.add(os);
		this.incrementeNbrItem();
	}
	
	private void incrementeNbrItem() {
		if (getListeObjets().get(this.getListeObjets().size()-1) instanceof Clef) {
			int somme = this.getNbrItem() + 1;
			this.nbrItem.set(somme);
		}
	}

	public boolean getEnAction() {
		return this.enAction.get();
	}
	
	public SimpleBooleanProperty getEnActionProperty() {
		return this.enAction;
	}
	
	public void setEnAction(boolean b) {
		this.enAction.set(b);
	}
	
	public ArrayList<ObjetStatique> getListeObjets() {
		return this.listeObjets;
	}
	
	public SimpleIntegerProperty getNbrItemProperty() {
		return this.nbrItem;
	}

	public int getNbrItem() {
		return this.nbrItem.get();
	}
	
	public void setPvMax(int i) {
		this.pvMax.setValue(i);
	}
	
	public ArrayList<Etat> getListeEtats(){
		return this.listeEtats;
	}
	
	public void setEtat(Etat e) {
		this.etat=e;
	}
	
	public SimpleIntegerProperty getNumEtatProperty() {
		return this.numEtat;
	}
	
	public int getNumEtat() {
		return this.numEtat.get();
	}
	
	public Etat getEtat() {
		return this.etat;
	}
	
	public int getPVMax() {
		return this.pvMax.getValue();
	}
	
	public Déplacement getDéplacementNage() {
		return this.déplacementNage;
	}
	
	public String toString() {
		return "soweli";
	}
}


