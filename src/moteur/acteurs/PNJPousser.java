package moteur.acteurs;

import moteur.Jeu;

public class PNJPousser extends PNJ {

	public PNJPousser(Jeu jeu, int x, int y, int oX, int oY, String texte) {
		super(jeu, x, y, oX, oY, texte);
	}
	
	public void reagir() {
		super.reagir();
		this.getJeu().getSoweli().setPeutPousser();
	}
	

}
