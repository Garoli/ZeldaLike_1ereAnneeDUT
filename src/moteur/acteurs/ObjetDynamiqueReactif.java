package moteur.acteurs;

import moteur.Jeu;

public abstract class ObjetDynamiqueReactif extends ObjetDynamique{

	public ObjetDynamiqueReactif(Jeu jeu, int x, int y, int oX, int oY,int vitesse) {
		super(jeu, x, y, oX, oY,vitesse);
	}
	public ObjetDynamiqueReactif(Jeu jeu, int x, int y, int oX, int oY) {
		super(jeu, x, y, oX, oY,0);
	}
	
	
	public abstract void reagir();

}
