package moteur.acteurs;

import moteur.Jeu;

public class Projectile extends Ennemi{
	
	public Projectile(Jeu jeu, int x, int y,int oX, int oY) {
		super(jeu, x, y, oX, oY, 30, 1, 12);
	}

	@Override
	public void agir() {
		if(!this.getDéplacement().deplacementBase()) {
			this.setPV(0);
		}
		else {
			this.attaque();
			this.addPV(-1);
		}
	}
	
	@Override
	public void attaque() {
		Ennemi e = this.getJeu().getCollision().tapeEnnemi(this);
		if(e!=null) {
			e.recoitDegat(e.getPtAttaque());
		}
	}
	
	public String toString() {
		return "projectile goutte";
	}
}
