package moteur.acteurs;

import javafx.beans.property.SimpleIntegerProperty;
import moteur.Jeu;

public class Acteur {

	private SimpleIntegerProperty x, y;
	private Jeu jeu;
	
	public Acteur(Jeu jeu, int x, int y) {
		this.x = new SimpleIntegerProperty(x);
		this.y = new SimpleIntegerProperty(y);
		this.jeu=jeu;
	}
	
	public Jeu getJeu() {
		return this.jeu;
	}
	
	public int getX() {
		return this.x.get();
	}
	
	public int getY() {
		return this.y.get();
	}
	
	public void setX(int x) {
		this.x.set(x);
	}
	
	public void setY(int y) {
		this.y.set(y);
	}
	
	public SimpleIntegerProperty getXProperty() {
		return this.x;
	}
	
	public SimpleIntegerProperty getYProperty() {
		return this.y;
	}
	
}