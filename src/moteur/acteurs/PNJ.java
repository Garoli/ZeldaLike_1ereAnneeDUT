package moteur.acteurs;

import moteur.Jeu;

public class PNJ extends ObjetDynamiqueReactif{

	private String texte;
	
	public PNJ(Jeu jeu, int x, int y, int oX, int oY, String texte) {
		super(jeu, x, y, oX, oY);
		this.texte=texte;
	}
	
	@Override
	public void reagir() {
		this.getJeu().setMessage(texte);	
	}
}
 