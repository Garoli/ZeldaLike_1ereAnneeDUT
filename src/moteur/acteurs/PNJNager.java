package moteur.acteurs;

import moteur.Jeu;

public class PNJNager extends PNJ {

	public PNJNager(Jeu jeu, int x, int y, int oX, int oY, String texte) {
		super(jeu, x, y, oX, oY, texte);
	}
	
	public void reagir() {
		super.reagir();
		this.getJeu().getSoweli().setPeutNager();
	}

}
