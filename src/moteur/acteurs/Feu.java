package moteur.acteurs;

import moteur.Jeu;

public class Feu extends Ennemi{

	public Feu(Jeu jeu, int x, int y) {
		super(jeu, x, y, 0, 0, 1, 1, 0);
	}

	@Override
	public void agir() {
		this.attaque();
	}
	
	@Override
	public void attaque() {
		if(this.getJeu().getTemps()%5==0 && this.getJeu().getCollision().toucheSoweli(this))
			this.getJeu().getSoweli().recoitDegat(this.getPtAttaque());
	}
	
}
