package moteur.acteurs;

import moteur.Jeu;
import moteur.etats.Etat;

public class PNJEtat extends PNJ {
	
	private Etat etat;
	public PNJEtat(Jeu jeu, int x, int y, int oX, int oY, String texte, Etat etat) {
		super(jeu, x, y, oX, oY, texte);
		this.etat=etat;
	}
	
	public void reagir() {
		super.reagir();
		if(this.etat!=null) {
			this.getJeu().getSoweli().addEtat(this.etat);
			this.etat=null;
		}
		
	}
	
	

}
