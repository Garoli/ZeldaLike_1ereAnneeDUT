package moteur.acteurs;

import javafx.beans.property.SimpleIntegerProperty;
import moteur.Jeu;
import moteur.déplacements.Déplacement;

public abstract class Personnage extends ObjetDynamique{
	
	private SimpleIntegerProperty pv;
	private int ptAttaque;
	private Déplacement déplacement;
	

	public Personnage(Jeu jeu, int x, int y, int oX, int oY,int pv,int ptAttaque, int vitesse) {
		super(jeu,x,y,oX,oY, vitesse);
		this.pv = new SimpleIntegerProperty(pv);
		
		this.ptAttaque=ptAttaque;
	}

	public abstract void agir();
	
	public void setDéplacement(Déplacement déplacement) {
		this.déplacement = déplacement;
	}

	public Déplacement getDéplacement() {
		return this.déplacement;
	}

	public int getPtAttaque() {
		return this.ptAttaque;
	}
	
	public SimpleIntegerProperty getPVProperty() {
		return this.pv;
		
	}
	public void addPV(int pv) {
		this.setPV(this.getPV()+pv);
	}
	
	public int getPV() {
		return this.pv.get();
	}
	
	public void setPV(int i) {
		this.pv.set(i);
	}
	
	public void recoitDegat (int pv) {
		this.pv.set(this.pv.get()-pv);
	}
	
	public String toString() {
		return "personnage ";
	}
}
