package moteur.acteurs;

import moteur.Jeu;
import moteur.déplacements.DéplacementLigne;

public class Souris extends Ennemi{

	public Souris(Jeu jeu,int x, int y, int oX, int Oy) {
		super(jeu, x, y, oX, Oy, 4,1,12);
		this.setDéplacement(new DéplacementLigne(jeu, this));
	}

	public String toString() {
		return super.toString() + "souris";
	}

	@Override
	public void agir() {
		this.getDéplacement().déplacer();
		this.attaque();
	}
}
