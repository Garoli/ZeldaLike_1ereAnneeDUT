package moteur.acteurs;

import moteur.Collision;
import moteur.Jeu;

public abstract class ObjetStatique extends Acteur{
	
	protected Collision collision;
	
	public ObjetStatique(Jeu jeu, int x, int y) {
		super(jeu, x, y);
		this.collision = jeu.getCollision();
	}
	
}
