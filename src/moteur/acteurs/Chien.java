package moteur.acteurs;

import moteur.Jeu;
import moteur.déplacements.DéplacementBFS;

public class Chien extends Ennemi{

	public Chien(Jeu jeu, int x, int y, int oX, int oY) {
		super(jeu, x, y, oX, oY, 12,4,12);
		this.setDéplacement(new DéplacementBFS(jeu, this));
	}

	@Override
	public void agir() {
		this.setEnMouvement(true);
		this.getDéplacement().déplacer();
		this.setEnMouvement(false);

		if (this.getJeu().getTemps() % 10 == 0) {
			this.attaque();
		}
	}

	public String toString() {
		return super.toString() + "chien";
	}
}
