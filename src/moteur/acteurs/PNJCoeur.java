package moteur.acteurs;

import moteur.Jeu;

public class PNJCoeur extends PNJ{

	private boolean donneDejaCoeur;
	
	public PNJCoeur(Jeu jeu, int x, int y, int oX, int oY, String texte) {
		super(jeu, x, y, oX, oY, texte);
		this.donneDejaCoeur = false;
	}
	
	@Override
	public void reagir() {
		super.reagir();
		if (! this.donneDejaCoeur) {
			this.getJeu().getSoweli().setPvMax(this.getJeu().getSoweli().getPVMax()+2);
			this.getJeu().getSoweli().addPV(2);
			this.donneDejaCoeur = true;
		}
	}
	
	
	

}
