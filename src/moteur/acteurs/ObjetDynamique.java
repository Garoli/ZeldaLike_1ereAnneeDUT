package moteur.acteurs;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import moteur.Jeu;
import moteur.Point2D;

public class ObjetDynamique extends Acteur{
	
	private SimpleIntegerProperty oX,oY;
	private int vitesse;
	private SimpleBooleanProperty enMouvement;
	
	public ObjetDynamique(Jeu jeu,int x, int y, int oX, int oY, int vitesse) {
		super(jeu,x,y);
		this.oX = new SimpleIntegerProperty(oX);
		this.oY = new SimpleIntegerProperty(oY);
		this.vitesse=vitesse;
		this.enMouvement=new SimpleBooleanProperty(false);
	}
	
	public void setEnMouvement(boolean b) {
		this.enMouvement.set(b);
	}
	
	public int getVitesse() {
		return this.vitesse;
	}
	
	public boolean getEnMouvement() {
		return this.enMouvement.get();
	}
	
	public SimpleBooleanProperty getEnMouvementProperty() {
		return this.enMouvement;
	}
	public int getOX() {
		return this.oX.getValue();
	}
	
	public int getOY() {
		return this.oY.getValue();
	}

	public SimpleIntegerProperty getOXProperty() {
		return this.oX;
	}
	
	public SimpleIntegerProperty getOYProperty() {
		return this.oY;
	}
	
	public void setOX(int x) {
		this.oX.setValue(x);
	}
	
	public void setOY(int y) {
		this.oY.setValue(y);
	}
	
	public void setOXetOY(int ox, int oy) {
		this.oX.setValue(ox);
		this.oY.setValue(oy);

	}
	
	public void changeOrientation(Point2D o) {
		this.oX.set(o.getX());
		this.oY.set(o.getY());
	}

}

