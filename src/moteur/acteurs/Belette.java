package moteur.acteurs;

import moteur.Jeu;
import moteur.déplacements.DéplacementCroix;

public class Belette extends Ennemi{

	private Jeu jeu;
	
	public Belette(Jeu jeu, int x, int y, int oX, int oY) {
		super(jeu,x, y, oX, oY, 8,2,8);
		this.jeu=jeu;

		this.setDéplacement(new DéplacementCroix(jeu, this));
	}
	
	@Override
	public void agir() {
		if (Math.abs(this.getX() - jeu.getSoweli().getX()) > Math.abs(this.getY() - jeu.getSoweli().getY())) {
			if(jeu.getSoweli().getX() > this.getX()) {
				this.setOXetOY(getVitesse(), 0);
			}
			else {

				this.setOXetOY(-getVitesse(),0);
			}

		}
		else {
			if (jeu.getSoweli().getY() > this.getY()) {
				this.setOXetOY(0, getVitesse());
			}
			else {

				this.setOXetOY(0, -getVitesse());
			}
		}
		
		this.getDéplacement().déplacer();
		
		if (this.jeu.getTemps() % 10 == 0) {
			this.attaque();
		}
	}
	
	public String toString() {
		return "belette";
	}

}
