package moteur.déplacements;

import moteur.Jeu;
import moteur.acteurs.ObjetDynamique;

public class DéplacementBase extends Déplacement{

	public DéplacementBase(Jeu jeu, ObjetDynamique obj) {
		super(jeu, obj);
	}

	@Override
	public void déplacer() {
		this.deplacementBase();
	}

	
}