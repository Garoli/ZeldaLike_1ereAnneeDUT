package moteur.déplacements;

import moteur.Jeu;
import moteur.acteurs.ObjetDynamique;

public class DéplacementLigne extends Déplacement {

	public DéplacementLigne(Jeu jeu, ObjetDynamique o) {
		super(jeu, o);
	}

	@Override
	public void déplacer() {
		if(this.getDéplacé().getJeu().getTemps()%2 ==0) {
			this.getDéplacé().setEnMouvement(true);

			if(this.getCollision().peutSeDeplacer(this.getDéplacé())) {
				deplacementBase();
			}
			else {
				this.getDéplacé().setOX(-this.getDéplacé().getOX());
				this.getDéplacé().setOY(-this.getDéplacé().getOY());
			}
			
			this.getDéplacé().setEnMouvement(false);
		}
	}

}
