package moteur.déplacements;

import moteur.Jeu;
import moteur.acteurs.ObjetDynamique;

public class DéplacementCroix extends Déplacement{
	
	public DéplacementCroix(Jeu jeu, ObjetDynamique obj) {
		super(jeu, obj);
	}

	@Override
	public void déplacer() {
		if (Math.abs(this.getDéplacé().getY()-this.getSoweli().getY())<600
				&& Math.abs(this.getDéplacé().getX()-this.getSoweli().getX())<600) {
			
			if( Math.abs(this.getDéplacé().getY()-this.getSoweli().getY())<48
					|| Math.abs(this.getDéplacé().getX()-this.getSoweli().getX())<48) {
				this.getDéplacé().setEnMouvement(true);
				this.deplacementBase();
			}
			this.getDéplacé().setEnMouvement(false);
		}
	}

	
}