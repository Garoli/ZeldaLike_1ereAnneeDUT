package moteur.déplacements;

import moteur.BFS;
import moteur.Jeu;
import moteur.Point2D;
import moteur.acteurs.ObjetDynamique;

public class DéplacementBFS extends Déplacement {

	private BFS bfs;

	public DéplacementBFS (Jeu jeu, ObjetDynamique objetQueJeDéplace) {
		super(jeu, objetQueJeDéplace);
		this.bfs = jeu.getBFS();
	}

	@Override
	public void déplacer() {
		if(this.getDéplacé().getJeu().getTemps()%2 ==0
				&& Math.abs(this.getDéplacé().getY()-this.getSoweli().getY())<2000
				&& Math.abs(this.getDéplacé().getX()-this.getSoweli().getX())<2000) {
			
			Point2D o = this.bfs.directionAPrendre(this.getDéplacé());
			this.getDéplacé().setEnMouvement(true);
			this.getDéplacé().changeOrientation(o.facteur(this.getDéplacé().getVitesse()));
			
			if( ! this.deplacementBase() || (o.getX()==0 && o.getY()==0)) {
				this.sePlacerDansLeCarré();
			}
			this.getDéplacé().setEnMouvement(false);
		}
	}

	private void sePlacerDansLeCarré() {
		Point2D o;
		
		if(this.getDéplacé().getX()%48!=0) {
			o = new Point2D(-1, 0);
		} else {
			o = new Point2D(0, -1);
		}
		this.getDéplacé().changeOrientation(o.facteur(6));
		this.deplacementBase();
	}

}