package moteur.déplacements;

import moteur.Jeu;
import moteur.acteurs.ObjetDynamique;
import moteur.Collision;

public abstract class Déplacement {
	
	private Collision collision;
	private ObjetDynamique soweli;
	private ObjetDynamique objetDéplacé;
	
	public Déplacement(Jeu jeu, ObjetDynamique obj){
		this.collision=jeu.getCollision();
		this.objetDéplacé=obj;
		this.soweli=jeu.getSoweli();
	}
	
	public ObjetDynamique getDéplacé() {
		return objetDéplacé;
	}
	
	public ObjetDynamique getSoweli() {
		return this.soweli;
	}
	
	public Collision getCollision() {
		return collision;
	}

	public abstract void déplacer();
	
	public void setXY() {
		objetDéplacé.setX(objetDéplacé.getX()+objetDéplacé.getOX());
		objetDéplacé.setY(objetDéplacé.getY()+objetDéplacé.getOY());
	}
	
	public boolean deplacementBase() {
		if(this.collision.peutSeDeplacer(objetDéplacé)) {
			setXY();
			return true;
		}
		return false;
	}
	
}
