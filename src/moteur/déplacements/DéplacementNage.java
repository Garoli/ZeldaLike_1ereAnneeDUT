package moteur.déplacements;

import moteur.Jeu;
import moteur.acteurs.ObjetDynamique;

public class DéplacementNage extends Déplacement {

	public DéplacementNage(Jeu jeu, ObjetDynamique obj) {
		super(jeu, obj);
	}

	@Override
	public void déplacer() {
		if(this.getCollision().peutNager(getDéplacé()) ) {
			this.setXY();
		}
		
	}

}
