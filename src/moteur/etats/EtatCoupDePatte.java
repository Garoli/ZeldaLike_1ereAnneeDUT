package moteur.etats;


import moteur.Jeu;
import moteur.acteurs.Ennemi;

public class EtatCoupDePatte extends Etat{


	public EtatCoupDePatte(Jeu jeu) {
		super(jeu);
	}

	@Override
	public void agir() { 
		if(this.getSoweli().getEnMouvement())
			this.getSoweli().getDéplacement().déplacer();
		
		if (this.getEnAction()) {
			Ennemi e = this.getJeu().getCollision().tapeEnnemi();
			if(e != null) {
				e.recoitDegat(this.getJeu().getSoweli().getPtAttaque());
			}
		}
	}
	
	public String toString() {
		return "ECDP";
	}
	

}