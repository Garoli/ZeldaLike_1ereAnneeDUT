package moteur.etats;

import moteur.Collision;
import moteur.Jeu;
import moteur.acteurs.Soweli;

public abstract class Etat {
	
	private Jeu jeu;
	
	public Etat(Jeu jeu){
		this.jeu=jeu;
	}
	
	public abstract void agir();
	
	public Collision getCollison() {
		return this.jeu.getCollision();
	}
	
	public Soweli getSoweli() {
		return this.jeu.getSoweli();
	}
	
	public Jeu getJeu() {
		return this.jeu;
	}
	
	public boolean getEnAction() {
		return this.jeu.getSoweli().getEnAction();
	}
	
	public String toString() {
		return "E";
	}
	
}
