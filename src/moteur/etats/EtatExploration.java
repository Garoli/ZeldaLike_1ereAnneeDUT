package moteur.etats;

import moteur.Jeu;
import moteur.acteurs.Clef;
import moteur.acteurs.Fleur;
import moteur.acteurs.ObjetDynamiqueReactif;

public class EtatExploration extends Etat {

	public boolean peutPousser;

	public EtatExploration(Jeu jeu) {
		super(jeu);
		this.peutPousser = false;
	}

	@Override
	public void agir() {
		
		if(this.getSoweli().getEnMouvement())
			this.getSoweli().getDéplacement().déplacer();
		
		ObjetDynamiqueReactif o = this.getCollison().tapeObjetDynamiqueRéactif();
		if(o != null ) {
			o.reagir();
		}
		
		if( this.getCollison().tapeObjetStatic() instanceof Clef && this.getEnAction()) {
			this.getSoweli().addObjetStatic(this.getCollison().tapeObjetStatic());
		}
		
		if( this.getCollison().tapeObjetStatic() instanceof Fleur && this.getEnAction() && this.getSoweli().getPV() < this.getSoweli().getPVMax()) {
			this.getSoweli().addPV(2);
			this.getSoweli().addObjetStatic(this.getCollison().tapeObjetStatic());
		}
		
		this.getSoweli().setEnAction(false);

		if(this.getSoweli().estDansLEau()) {
			this.getSoweli().setEnAction(true);
		}
	}

	public String toString() {
		return "EE";
	}

	public void setPeutPousser(boolean peutPousser) {
		this.peutPousser = peutPousser;
	}
	
	public boolean getPeutPousser() {
		return peutPousser;
	}

}