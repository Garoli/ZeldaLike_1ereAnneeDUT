package moteur.etats;

import moteur.Jeu;
import moteur.acteurs.Projectile;
import moteur.déplacements.Déplacement;
import moteur.déplacements.DéplacementBase;


public class EtatEnvoieProjectile extends Etat{

	public EtatEnvoieProjectile(Jeu jeu) {
		super(jeu);
	}

	@Override
	public void agir() {
		if(this.getSoweli().getEnMouvement())
			this.getSoweli().getDéplacement().déplacer();
		
		if (this.getJeu().getTemps() % 3 == 0 && this.getEnAction()) {
			Projectile p = new Projectile(this.getJeu(), 
					this.getSoweli().getX()+3*this.getSoweli().getOX(),
					this.getSoweli().getY()+3*this.getSoweli().getOY(),
					this.getSoweli().getOX(),
					this.getSoweli().getOY());

			Déplacement d = new DéplacementBase(this.getJeu(), p);
			p.setDéplacement(d);
			this.getJeu().addEnnemi(p);
			this.getJeu().getCollision().addObjet(p);
		}
	}

	public String toString() {
		return "EP";
	}


}
