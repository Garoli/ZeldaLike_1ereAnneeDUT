package moteur.etats;

import moteur.Jeu;

public class EtatNager extends Etat {

	public EtatNager(Jeu jeu) {
		super(jeu);

	}

	@Override
	public void agir() {
		this.getSoweli().setEnAction(false);
		if(this.getSoweli().getEnMouvement())
			this.getSoweli().getDéplacementNage().déplacer();
	
		
		if(this.getSoweli().estDansLEau())
			this.getSoweli().setEnAction(true);
	
	}
	
}