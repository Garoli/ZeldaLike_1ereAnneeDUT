package moteur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import moteur.Jeu;
import moteur.Map;
import moteur.Point2D;
import moteur.Tile;
import moteur.acteurs.ObjetDynamique;
import moteur.acteurs.Soweli;

public class BFS {

	private Soweli soweli;
	private Map map;
	private Collision collision;
	private ArrayList<Tile> sommets;
	private HashMap<Tile, ArrayList<Tile>> hashmap;
	private HashMap<Tile, Tile> bfs;

	public BFS (Jeu jeu) {
		this.map=jeu.getMap();
		this.soweli=jeu.getSoweli();
		this.collision=jeu.getCollision();
		this.hashmap = new HashMap<>();
		this.sommets = new ArrayList<>();

		Tile[][] points = this.map.getTerrain();

		for (int i=0; i<points.length; i++) {
			for (int j=0; j<points[0].length; j++) {

				Tile pd = points[i][j];
				this.sommets.add(pd);

				ArrayList<Tile> voisins = new ArrayList<>();

				if (i>0 && points[i-1][j].estAccessible()) {
					voisins.add(points[i-1][j]);
				}
				if (j>0 && points[i][j-1].estAccessible()) {
					voisins.add(points[i][j-1]);
				}
				if(i<points.length-1 && points[i+1][j].estAccessible()) {
					voisins.add(points[i+1][j]);
				}
				if(j<points[0].length-1 && points[i][j+1].estAccessible()) {
					voisins.add(points[i][j+1]);
				}
				this.hashmap.put(pd, voisins);
			}
		}
	}

	public void makeBFS() {
		
		Tile origine = this.map.getTile(this.soweli.getX(), this.soweli.getY());
		
		this.bfs = new HashMap<>();
		LinkedList<Tile> queue = new LinkedList<>();
		
		queue.add(origine);
		this.bfs.put(origine, null);
		
		Tile actif;
		while(!queue.isEmpty()) {
			
			actif =  queue.poll();
			
			if (this.collision.contientSoweli(actif) || this.collision.estLibre(actif)) {
				ArrayList<Tile> voisins = this.hashmap.get(actif);
				for (Tile v : voisins) {
					if (!bfs.containsKey(v) && this.collision.estLibre(actif)) {
						queue.add(v);
						this.bfs.put(v,actif);
					}
				}
			}
		}
	}

	public Point2D directionAPrendre(ObjetDynamique od) {

		Tile pA, pO = this.map.getTile(od.getX(), od.getY());
		try {
			pA = this.bfs.get(pO);
		}
		catch(NullPointerException e) {
			pO = this.map.getTile(od.getX()+48, od.getY());
			try {
				pA = this.bfs.get(pO);
			}
			catch(NullPointerException f) {
				pO = this.map.getTile(od.getX(), od.getY()+48);
				try {
					pA = this.bfs.get(pO);
				}
				catch(NullPointerException g) {
					pO = this.map.getTile(od.getX()+48, od.getY()+48);
					try {
						pA = this.bfs.get(pO);
					}
					catch(NullPointerException h) {
						pA = null;
					}	
				}	
			}
		}
		
		if(pA==null) {
			return new Point2D(0, 0);
		}
		return new Point2D(pA.getX()-pO.getX(), pA.getY()-pO.getY());
	}

}