package moteur;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import moteur.exceptions.LigneTropGrande;
import moteur.exceptions.LigneTropPetite;


public class Map {
	
	private int hauteur;
	private int largeur;
	private Tile[][] mieuxQueMappy;
	
	public Map(String adresse, int maxNumeroTile) throws LigneTropPetite, LigneTropGrande{
		
		 try {
	            Scanner scanner = new Scanner(new File(adresse));

	            scanner.useDelimiter(",");

	            String[] ligne1 = scanner.nextLine().split(",");
	            this.hauteur = Integer.parseInt(ligne1[0]);
	            this.largeur = Integer.parseInt(ligne1[1]);
	            
	            this.mieuxQueMappy = new Tile[hauteur][largeur];
	            
	            for(int i=0; i<hauteur; i++) {
	                String ligne = scanner.nextLine();
	                String[] tableauTile = ligne.split(",");
	                
	                for(int j=0; j<largeur; j++) {
	                    try{
	                    	try {
	                    		this.mieuxQueMappy[i][j] = new Tile(j, i, Integer.parseInt(tableauTile[j]));
	                    	}catch(NullPointerException e) {
	                   		throw new LigneTropPetite();
	                    }
	                    }catch(ArrayIndexOutOfBoundsException e){
	                    	this.mieuxQueMappy[i][j] = new Tile(j, i, 1);
	                    	throw new LigneTropGrande();
	                    }
	                    
	                }
	            }
	            scanner.close();
	        } catch (FileNotFoundException e) {
	        
	        }
		 
		 
		 
		 	
	}
	
	public Tile[][] getTerrain() {
		return mieuxQueMappy;
	}
	
	public Tile getTile(int x, int y) {
		if (x < 0 || y< 0 || y> (this.mieuxQueMappy.length-1)*48 || x > (this.mieuxQueMappy[0].length-1)*48 ) {
			return null;
		}
		if (this.mieuxQueMappy[y/48][x/48] == null)
			throw new Error("Tile inexistante:" + y/48 + "," + x/48);
		return this.mieuxQueMappy[y/48][x/48];
	}

	
	public boolean estAccessible(int x, int y) {
		if (x < 0 || y < 0 || y/48 >= this.mieuxQueMappy.length || x/48 >= this.mieuxQueMappy[0].length ) {
			return false;
		}
		return this.mieuxQueMappy[y/48][x/48].estAccessible();
	}
	
	public boolean estEau(int x, int y) {
		if (x < 0 || y < 0 || y/48 >= this.mieuxQueMappy.length || x/48 >= this.mieuxQueMappy[0].length ) {
			return false;
		}
		return this.mieuxQueMappy[y/48][x/48].estEau();
	}
	
	public int getHauteur() {
		return this.hauteur;
	}
	
	public int getLargeur() {
		return this.largeur;
	}

}
		
		
		
		
		