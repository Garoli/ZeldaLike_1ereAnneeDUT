package moteur;

import java.util.ArrayList;

import moteur.acteurs.Acteur;
import moteur.acteurs.Chien;
import moteur.acteurs.Ennemi;
import moteur.acteurs.ObjetDynamique;
import moteur.acteurs.ObjetDynamiqueReactif;
import moteur.acteurs.ObjetStatique;
import moteur.acteurs.Personnage;
import moteur.acteurs.Soweli;

public class Collision {

	private Jeu jeu;
	private Map map;
	private Soweli soweli;
	private ArrayList<Acteur> acteurs;
	/**
	 * Récupère la map
	 * Faire le addSoweli pour ajouter soweli et màjListeObjets pour ajouter tous les persos
	 * @param jeu
	 */
	public Collision(Jeu jeu) {
		this.jeu=jeu;
		this.map = jeu.getMap();
		this.acteurs = new ArrayList<>();
	}

	public void màJListeObjets() {
		this.acteurs.clear();
		this.acteurs.add(this.soweli);
		this.acteurs.addAll(jeu.getListeEnnemi());
		this.acteurs.addAll(jeu.getListeObjetDynamique());
		this.acteurs.addAll(jeu.getListeObjetStatique());
	}

	public void removeObjet(Acteur o) {
		this.acteurs.remove(o);
	}
	
	public void addObjet(Acteur o) {
		this.acteurs.add(o);
	}

	public void addSoweli(Soweli soweli) {
		this.soweli=soweli;
	}

	public boolean peutSeDeplacer(ObjetDynamique veutSeDéplacer) {
		if(this.tapeActeur(veutSeDéplacer)!=null) {
			return false;
		}

		int xArrive = veutSeDéplacer.getX()+veutSeDéplacer.getOX();
		int yArrive = veutSeDéplacer.getY()+veutSeDéplacer.getOY();

		if ( ! (this.map.estAccessible(xArrive, yArrive)
				&& this.map.estAccessible(xArrive+47, yArrive)
				&& this.map.estAccessible(xArrive, yArrive+47)
				&& this.map.estAccessible(xArrive+47, yArrive+47) ) ) {
			return false;
		}
		return true;
	}

	public boolean peutSeDeplacer() {
		return this.peutSeDeplacer(this.soweli);
	}

	public boolean peutNager(ObjetDynamique veutSeDéplacer) {
		if(this.tapeActeur(veutSeDéplacer)!=null) {
			return false;
		}

		int x = veutSeDéplacer.getX()+veutSeDéplacer.getOX();
		int y = veutSeDéplacer.getY()+veutSeDéplacer.getOY();

		if ( ! (this.nageable(x, y)
				&&this.nageable(x+47, y)
				&&this.nageable(x, y+47)
				&&this.nageable(x+47, y+47)) ) {
			return false;
		}
		return true;
	}
	
	public boolean peutNager() {
		return this.peutNager(this.soweli);
	}
	
	private boolean nageable(int x, int y) {
		return this.map.estEau(x, y) || this.map.estAccessible(x, y);
	}


	private Acteur tapeActeur(ObjetDynamique actif, int coef) {
		for(Acteur passif : this.acteurs) {
			int x = actif.getX() + actif.getOX()*coef;
			int y = actif.getY() + actif.getOY()*coef;
			if(passif!=actif
					&& Math.abs(x-passif.getX())<48 && Math.abs(y-passif.getY())<48){ 
				return passif;
			}
		}
		return null;
	}	

	public Acteur tapeActeur(ObjetDynamique objetDynamique) {
		return tapeActeur(objetDynamique, 1);
	}

	public Ennemi tapeEnnemi(ObjetDynamique o) {
		Acteur e = tapeActeur(o,2);
		if(e instanceof Ennemi) {
			return (Ennemi) e;
		}
		return null;
	}
	
	public Ennemi tapeEnnemi() {
		return tapeEnnemi(this.soweli);
	}

	public ObjetStatique tapeObjetStatic() {
		Acteur e = tapeActeur(this.soweli,1);
		if(e instanceof ObjetStatique) {
			return (ObjetStatique) e;
		}
		return null;
	}
	
	public ObjetDynamiqueReactif tapeObjetDynamiqueRéactif() {
		Acteur e = tapeActeur(this.soweli);
		if(e instanceof ObjetDynamiqueReactif) {
			return (ObjetDynamiqueReactif) e;
		}
		return null;
	}

	public boolean tapeSoweli(Personnage pQuiVeutSeDéplacer) {
		if( tapeActeur(pQuiVeutSeDéplacer)==this.soweli) {
			return true;
		}
		return false;
	}
	
	public boolean toucheSoweli(Personnage p) {
		if (Math.abs(this.soweli.getX()-p.getX())<60 && Math.abs(this.soweli.getY()-p.getY())<60)
				return true;
		return false;
	}

	public boolean estLibre(Tile t) {
		for(Acteur a : this.jeu.getListeObjetDynamique()) {
			if (Math.abs(t.getX()*48-a.getX())<48 
			 && Math.abs(t.getY()*48-a.getY())<48) {
				return false;
			}
		}
		for(Acteur a : this.jeu.getListeEnnemi()) {
			if (! (a instanceof Chien) 
					&& Math.abs(t.getX()*48-a.getX())<48 
					&& Math.abs(t.getY()*48-a.getY())<48) {
				return false;
			}
		}
		return true;
	}
	public boolean contientSoweli(Tile t) {
		if(Math.abs(this.soweli.getX()-t.getX())<48 && Math.abs(this.soweli.getY()-t.getY())<48)
			return true;
		return false;
	}
}

