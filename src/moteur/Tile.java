package moteur;

public class Tile extends Point2D{

	private int id;
	private boolean accessible;
	private boolean eau;
	private boolean marqué;

	public Tile(int x, int y, int id) {
		super(x,y);
		this.id=id;
		this.accessible=traversable(id);
		this.marqué=false;
		this.eau=aqueux(id);
	}

	private boolean traversable(int idPoint) {
		if (idPoint == 1)
			return true;
		return false;
	}
	private boolean aqueux(int idPoint) {
		if(idPoint >= 56 &&  idPoint <= 61)
			return true;
		return false;
	}

	public int getID() {
		return this.id;
	}

	public boolean estAccessible() {
		return this.accessible;
	}
	
	public boolean estEau() {
		return this.eau;
	}

	public boolean estMarqué() {
		return this.marqué;
	}
	
	public void marquer() {
		this.marqué=true;
	}
	public void démarquer() {
		this.marqué=false;
	}

	public String toString() {
		return "tile : x=" + getX() + ", y=" + getY() + "\n" /*+ ", id=" + id + ", accessible=" + accessible*/;
	}

}
