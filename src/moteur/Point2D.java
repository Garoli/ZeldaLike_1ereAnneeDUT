package moteur;

public class Point2D {

	private int x,y;

	public Point2D(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public Point2D facteur(int facteur) {
		return new Point2D(facteur*x, facteur*y);
	}
	
	@Override
	public String toString() {
		return "orientation : x=" + this.x + ", y=" + this.y;
	}
}
