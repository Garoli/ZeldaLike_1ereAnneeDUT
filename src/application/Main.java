package application;

import java.io.File;
import java.net.URL;

import controleur.Controleur;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader();
			URL url = new File("src/vue/Vue.fxml").toURI().toURL();
			loader.setLocation(url);
			Pane root=loader.load();
			Scene scene = new Scene(root,root.getPrefWidth(),root.getPrefHeight());
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.setTitle("The Legend Of Soweli");
			primaryStage.show();
			Controleur controleur = loader.getController();
			controleur.setStage(primaryStage);
			scene.setOnKeyPressed(e -> controleur.touchepresse(e));
			scene.setOnKeyReleased(e -> controleur.toucherelachee());
			scene.setOnKeyTyped(e -> controleur.touchetapee(e));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
