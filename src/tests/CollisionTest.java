package tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import controleur.Fabrique;
import moteur.*;
import moteur.acteurs.*;

public class CollisionTest {

	private Jeu jeu;
	private Collision c;
	private Soweli s;
	private Ennemi e;

	@Before
	public final void beforePuisSoiréePuisAfterEtPuisTechnoHouseTranceToutÇaToutÇa() {
		try {
			this.jeu = Fabrique.fabriqueDeJeuMap1();
			this.c = jeu.getCollision();
			this.s = jeu.getSoweli();
			this.e = jeu.getEnnemi(0);
		} catch (Exception e) {

		}
	}

	@Test
	public final void testPeutSeDéplacersurBords() {
		//bord gauche
		
		s.setX(0);
		s.setY(2*48);
		//oriente vers le haut
		s.setOXetOY(0, -1);
		assertTrue(c.peutSeDeplacer());
		//gauche
		s.setOXetOY(-1, 0);
		assertFalse(c.peutSeDeplacer());
		//bas
		s.setOXetOY(0,1);
		assertTrue(c.peutSeDeplacer());
		//droite
		s.setOXetOY(1,0);
		assertTrue(c.peutSeDeplacer());

		//bord haut
		
		s.setX(2*48);
		s.setY(0);
		//oriente vers le haut
		s.setOXetOY(0, -1);
		assertFalse(c.peutSeDeplacer());
		//gauche
		s.setOXetOY(-1, 0);
		assertTrue(c.peutSeDeplacer());
		//bas
		s.setOXetOY(0,1);
		assertTrue(c.peutSeDeplacer());
		//droite
		s.setOXetOY(1,0);
		assertTrue(c.peutSeDeplacer());

		//bord droit
		
		s.setX(19*48);
		s.setY(2*48);
		//oriente vers le haut
		s.setOXetOY(0, -1);
		assertTrue(c.peutSeDeplacer());
		//gauche
		s.setOXetOY(-1, 0);
		assertTrue(c.peutSeDeplacer());
		//bas
		s.setOXetOY(0,1);
		assertTrue(c.peutSeDeplacer());
		//droite
		s.setOXetOY(1,0);
		assertFalse(c.peutSeDeplacer());

		//bord bas
		
		s.setX(2*48);
		s.setY(14*48);
		//oriente vers le haut
		s.setOXetOY(0, -1);
		assertTrue(c.peutSeDeplacer());
		//gauche
		s.setOXetOY(-1, 0);
		assertTrue(c.peutSeDeplacer());
		//bas
		s.setOXetOY(0,1);
		assertFalse(c.peutSeDeplacer());
		//droite
		s.setOXetOY(1,0);
		assertTrue(c.peutSeDeplacer());
	}

	public final void testPeutSeDéplacerSurObstaclesMap() {
		//obstacle gauche
		
		s.setX(6*48);
		s.setY(8*48);
		//oriente vers le haut
		s.setOXetOY(0, -1);
		assertTrue(c.peutSeDeplacer());
		//gauche
		s.setOXetOY(-1, 0);
		assertFalse(c.peutSeDeplacer());
		//bas
		s.setOXetOY(0,1);
		assertTrue(c.peutSeDeplacer());
		//droite
		s.setOXetOY(1,0);
		assertTrue(c.peutSeDeplacer());

		//obstacle haut
		
		s.setX(5*48);
		s.setY(10*48);
		//oriente vers le haut
		s.setOXetOY(0, -1);
		assertFalse(c.peutSeDeplacer());
		//gauche
		s.setOXetOY(-1, 0);
		assertTrue(c.peutSeDeplacer());
		//bas
		s.setOXetOY(0,1);
		assertTrue(c.peutSeDeplacer());
		//droite
		s.setOXetOY(1,0);
		assertTrue(c.peutSeDeplacer());

		//obstacle droit
		
		s.setX(12*48);
		s.setY(8*48);
		//oriente vers le haut
		s.setOXetOY(0, -1);
		assertTrue(c.peutSeDeplacer());
		//gauche
		s.setOXetOY(-1, 0);
		assertTrue(c.peutSeDeplacer());
		//bas
		s.setOXetOY(0,1);
		assertTrue(c.peutSeDeplacer());
		//droite
		s.setOXetOY(1,0);
		assertFalse(c.peutSeDeplacer());

		//obstacle bas
		
		s.setX(10*48);
		s.setY(10*48);
		//oriente vers le haut
		s.setOXetOY(0, -1);
		assertTrue(c.peutSeDeplacer());
		//gauche
		s.setOXetOY(-1, 0);
		assertTrue(c.peutSeDeplacer());
		//bas
		s.setOXetOY(0,1);
		assertFalse(c.peutSeDeplacer());
		//droite
		s.setOXetOY(1,0);
		assertTrue(c.peutSeDeplacer());
	}

	@Test
	public final void testPeutSeDéplacerSurEnnemi() {
		s.setX(3*48);
		s.setY(3*48);

		//cas 0 : pas d'ennemi
		
		e.setX(0);
		e.setY(0);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//gauche
		s.setOXetOY(-12, 0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//bas
		s.setOXetOY(0,12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//droite
		s.setOXetOY(12,0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);

		//cas ennemi à gauche
		
		e.setX(2*48);
		e.setY(3*48);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//gauche
		s.setOXetOY(-12, 0);
		assertFalse(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==e);
		//bas
		s.setOXetOY(0,12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//droite
		s.setOXetOY(12,0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);

		//cas ennemi à gauche éloigné
		e.setX(2*48-12);
		e.setY(3*48);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//gauche
		s.setOXetOY(-12, 0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==e);
		//bas
		s.setOXetOY(0,12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//droite
		s.setOXetOY(12,0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);

		//cas ennemi à droite
		
		e.setX(4*48);
		e.setY(3*48);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//gauche
		s.setOXetOY(-12, 0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//bas
		s.setOXetOY(0,12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//droite
		s.setOXetOY(12,0);
		assertFalse(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==e);

		//cas ennemi à droite éloigné
		e.setX(4*48+12);
		e.setY(3*48);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//gauche
		s.setOXetOY(-12, 0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//bas
		s.setOXetOY(0,12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//droite
		s.setOXetOY(12,0);
		assertTrue(c.tapeEnnemi()==e);
		assertTrue(c.peutSeDeplacer());

		//cas ennemi en haut
		
		e.setX(3*48);
		e.setY(2*48);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertFalse(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==e);
		//gauche
		s.setOXetOY(-12, 0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//bas
		s.setOXetOY(0,12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//droite
		s.setOXetOY(12,0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);

		//cas ennemi en haut éloigné
		e.setX(3*48);
		e.setY(2*48-12);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==e);

		//gauche
		s.setOXetOY(-12, 0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//bas
		s.setOXetOY(0,12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//droite
		s.setOXetOY(12,0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);

		//cas ennemi en bas
		
		e.setX(3*48);
		e.setY(4*48);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//gauche
		s.setOXetOY(-12, 0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//bas
		s.setOXetOY(0,12);
		assertFalse(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==e);
		//droite
		s.setOXetOY(12,0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);

		//cas ennemi en bas éloigné
		e.setX(3*48);
		e.setY(4*48+12);
		//oriente vers le haut
		s.setOXetOY(0, -12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//gauche
		s.setOXetOY(-12, 0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);
		//bas
		s.setOXetOY(0,12);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==e);
		//droite
		s.setOXetOY(12,0);
		assertTrue(c.peutSeDeplacer());
		assertTrue(c.tapeEnnemi()==null);

	}

	@Test
	public final void testEstLibre() {
		//Vérification sans ennemi

		e.setX(0);
		e.setY(0);

		assertTrue(c.estLibre(new Tile(7, 7, 1)));
		assertTrue(c.estLibre(new Tile(7, 8, 1)));
		assertTrue(c.estLibre(new Tile(8, 7, 1)));
		assertTrue(c.estLibre(new Tile(8, 8, 1)));

		//Ennemi à cheval au milieu

		e.setX(8*48-24);
		e.setY(8*48-24);

		assertFalse(c.estLibre(new Tile(7, 7, 1)));
		assertFalse(c.estLibre(new Tile(7, 8, 1)));
		assertFalse(c.estLibre(new Tile(8, 7, 1)));
		assertFalse(c.estLibre(new Tile(8, 8, 1)));

		//Ennemi à cheval en décalé

		e.setX(8*48-36);
		e.setY(8*48-12);

		assertFalse(c.estLibre(new Tile(7, 7, 1)));
		assertFalse(c.estLibre(new Tile(7, 8, 1)));
		assertFalse(c.estLibre(new Tile(8, 7, 1)));
		assertFalse(c.estLibre(new Tile(8, 8, 1)));
	}

}
